﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class TrumpController : MonoBehaviour {

	float speed=2f;
	Animation animation;
	Rigidbody rb;
	void Start () {
		animation=GetComponent<Animation>();
		rb=GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		float x = CrossPlatformInputManager.GetAxis("Horizontal");
		float y = CrossPlatformInputManager.GetAxis("Vertical");
		Vector3 movement=new Vector3(x,0,y); 
		rb.velocity=speed*movement;
		if(!x.Equals(0)&&!y.Equals(0))
		{
			transform.eulerAngles=new Vector3(transform.eulerAngles.x, Mathf.Atan2(x,y)*Mathf.Rad2Deg,transform.eulerAngles.z);
		}
		if(!x.Equals(0)||!y.Equals(0))
		{
			//transform.position+=transform.forward*Time.deltaTime*speed;
			animation.Play("Walk");
		}
		else
		{
			animation.Play("Idle");
		}
	}

}

